const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()

const TOKEN_SECRET = process.env.TOKEN_SECRET


const mysql = require('mysql')


const connection = mysql.createConnection({
    hosst : 'localhost',
    user: 'dperm',
    password : 'dperm',
    database: 'CakeReviewSystem',
    multipleStatements: true
})





connection.connect();

const express = require('express')
const app =express()
const port = 4000




/*Middle Ware for Authenticating user token*/

function authenticateToken(req, res, next) {
    const authenHeader = req.headers['authorization']
    const token = authenHeader && authenHeader.split(' ')[1]
    
    if(token == null) {
        return res.sendStatus(401)
    }
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err) {
            return res.sendStatus(403)
        }else {
            req.user = user
            next()
        }
    })
}






//list of product by ID
app.get("/list_by_product_id",(req,res) =>{

    let product_id = req.query.product_id

    let query = `
    
     UPDATE CakeProduct
    SET ProductRating = (SELECT AVG(GivenRating) 
    FROM Rating WHERE ProductID = ${product_id})
    WHERE ProductID = ${product_id};

    SELECT * FROM CakeProduct WHERE ProductID = ${product_id}`
    

                    


    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error listing CakeProduct from db"
                      })
        }else{
            res.json(rows);
        }

});

})

//list of rating

app.get("/list_rating",(req,res) =>{

    let query = "SELECT * FROM Rating";


    
    connection.query( query,(err,rows) => {
        if(err){
            res.json( { 

                "status" : "400",
                "message" : "Error listing Rating from db"
                      })
        }else{
            res.json(rows);
        }

});

})

//list of user

app.get("/list_user",(req,res) =>{

    let query = "SELECT * from Reviewer";

    connection.query( query,(err,rows) => {
        if(err){
            res.json( { 

                "status" : "400",
                "message" : "Error listing Reviewer from db"
                      })
        }else{
            res.json(rows);
        }

});

})

//insert product

app.post("/add_product" , (req,res) => {

    let product_name = req.query.product_name
    let product_price = req.query.product_price

    let query = `INSERT INTO CakeProduct 
                    (ProductName , ProductPrice) 
                    VALUES ('${product_name}','${product_price}')`
    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error Adding CakeProduct from db"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Adding CakeProduct success!"
            });
        }

});
})


//update product   

app.post("/update_product" , (req,res) => {

    let product_id = req.query.product_id
    let product_name = req.query.product_name
    let product_price = req.query.product_price

    let query = `UPDATE CakeProduct SET
                       ProductName = '${product_name}' ,
                       ProductPrice = '${product_price}'
                       WHERE ProductID = ${product_id}`
    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error updating CakeProduct from db"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Updating CakeProduct success!"
            });
        }

});
})

//delete product

app.post("/delete_product" , (req,res) => {

    let product_id = req.query.product_id

    let query = `DELETE FROM CakeProduct
                       WHERE ProductID = ${product_id}`
    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error Deleting CakeProduct record from db"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Deleting CakeProduct record success!"
            });
        }

});
})

//delete User

app.post("/delete_user" , (req,res) => {

    let user_id = req.query.user_id

    let query = `DELETE FROM Reviewer
                       WHERE UserID = ${user_id}`
    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error Deleting User from db"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Deleting User successful!"
            });
        }

});
})

// API for processing user Authorization
app.post("/login",(req,res)=>{
    let username = req.query.username
    let password = req.query.password

    let query = `SELECT * FROM Reviewer WHERE Username = '${username}'`
    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "ERROR"
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(password, db_password,(err,result)=>{
                if(result == true){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].UserID,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(TOKEN_SECRET)
                    console.log(payload)
                    let token = jwt.sign(payload,TOKEN_SECRET,{expiresIn : '1d'})
                    res.send(token)
                    
                }else{res.send("Invalid username / password")}
            })
        }
    })

})


//register account

app.post("/register" , (req,res) => {

    let username = req.query.username
    let password = req.query.password
    

    bcrypt.hash(password, SALT_ROUNDS,(err,hash) =>{

        let query = `INSERT INTO Reviewer 
                        (Username , Password,IsAdmin) 
                        VALUES ('${username}','${hash}',false)`
    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error registering"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Register success!"
            });
        }

        
});


    })



    
})

//insert rating

app.post("/rating" , authenticateToken,(req,res) => {
    

    let rating_score = req.query.rating_score
    let comment = req.query.comment
    let product_id = req.query.product_id
    let user_id = req.query.user_id

    let query = `INSERT INTO Rating 
                    (ProductID,UserID,GivenRating , Commenting ) 
                    VALUES ('${product_id}','${user_id}',${rating_score},'${comment}')
    `




    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error Giving Rating Score"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Giving Rating Score successful!"
            });
        }

});
})

//update password

app.post("/update_password" , authenticateToken,(req,res) => {

    let password = req.query.password
    let user_id = req.query.user_id

    let query = `UPDATE Reviewer SET
                       Password = '${password}'
                       WHERE UserID = ${user_id}`
    console.log(query)

    connection.query( query,(err,rows) => {
        if(err){
            console.log(err)
            res.json( { 

                "status" : "400",
                "message" : "Error updating password from db"
                      })
        }else{
            
            res.json({
                "status" : "200",
                "message" : "Updating password success!"
            });
        }

});
})

app.listen(port,() => {
    console.log(`Now starting Cake Review System at port ${port} `)
})

/*query = "SELECT * from CakeProduct";
connection.query( query,(err,rows) => {
    if(err){
        console.log(err);
    }else{
        console.log(rows);
    }

});

connection.end();
*/